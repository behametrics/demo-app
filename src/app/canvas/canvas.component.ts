import {
  Component,
  OnInit,
  ViewChild,
  ElementRef,
  Output,
  EventEmitter
} from '@angular/core';

@Component({
  selector: 'app-canvas',
  templateUrl: './canvas.component.html',
  styleUrls: ['./canvas.component.scss']
})
export class CanvasComponent implements OnInit {
  @ViewChild('canvas')
  canvas: ElementRef;

  @Output()
  drawEnd = new EventEmitter<any>();

  context: CanvasRenderingContext2D;
  isDrawing = false;
  lastX = 0;
  lastY = 0;

  ngOnInit() {
    this.context = this.canvas.nativeElement.getContext('2d');
    this.canvas.nativeElement.width = 500;
    this.canvas.nativeElement.height = 500;
    this.context.strokeStyle = 'black';
    this.context.lineJoin = 'round';
    this.context.lineCap = 'round';
    this.context.lineWidth = 3;
  }

  draw(event: MouseEvent) {
    if (!this.isDrawing) {
      return;
    }
    this.context.beginPath();
    this.context.moveTo(this.lastX, this.lastY);
    this.context.lineTo(event.offsetX, event.offsetY);
    this.context.stroke();
    this.lastX = event.offsetX;
    this.lastY = event.offsetY;
  }

  clear() {
    this.context.clearRect(
      0,
      0,
      this.canvas.nativeElement.width,
      this.canvas.nativeElement.height
    );
  }

  onMouseDown(event: MouseEvent) {
    this.isDrawing = true;
    this.lastX = event.offsetX;
    this.lastY = event.offsetY;
  }

  onMouseUp() {
    this.isDrawing = false;
    this.drawEnd.emit();
  }

  onMouseOut() {
    this.isDrawing = false;
  }
}
