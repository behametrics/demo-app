import { Component, ViewChild } from '@angular/core';
import { ConnectionService } from './connection.service';
import { Logger } from 'fastar-logger';
import { delay } from 'rxjs/operators';
import { of } from 'rxjs';
import { CanvasComponent } from './canvas/canvas.component';

const API_URL = 'https://team04-18.studenti.fiit.stuba.sk/logger-server/prod';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent {
  logger: any;
  mode = 'Enter username and choose mode';
  userNameField = null;
  counter = 0;

  @ViewChild(CanvasComponent)
  canvas: CanvasComponent;

  constructor(private conn: ConnectionService) {
    this.logger = new Logger({ apiUrl: API_URL });
  }

  authenticate() {
    this.logger.stopLogging();
    this.conn
      .authenticateUser({
        sessionId: this.logger.sessionId,
        username: this.userNameField
      })
      .subscribe(data => {
        console.log(data);
        if (data.is_authenticated) {
          alert(`User ${data.username} is authenticated`);
        } else {
          alert(`User ${data.username} is not authenticated`);
        }
      });
  }

  onTrainMode() {
    if (!this.isInputValid()) {
      return;
    }
    this.mode = 'Train';
    this.logger.stopLogging();
    this.logger.init({ username: this.userNameField });
    this.logger.startLogging();
  }

  onAuthMode() {
    if (!this.isInputValid()) {
      return;
    }
    this.mode = 'Authentication';
    this.logger.stopLogging();
    this.logger.init({ username: this.userNameField });
    this.logger.startLogging();
  }

  isInputValid() {
    if (!this.userNameField || this.userNameField === '') {
      alert('Please enter username');
      return false;
    }
    return true;
  }

  onDrawEnd() {
    of(null)
      .pipe(delay(200))
      .subscribe(() => this.canvas.clear());
    this.counter++;
  }

  reset() {
    this.canvas.clear();
    this.counter = 0;
  }
}
