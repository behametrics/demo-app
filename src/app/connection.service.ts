import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { HttpClient } from '@angular/common/http';
import { take } from 'rxjs/operators';

const URL_AUTH = `https://team04-18.studenti.fiit.stuba.sk/demo/server/auth`;

@Injectable({
  providedIn: 'root'
})
export class ConnectionService {
  constructor(private http: HttpClient) {}

  authenticateUser(data: any): Observable<any> {
    return this.http
      .get(URL_AUTH, {
        params: {
          ...data
        }
      })
      .pipe(take(1));
  }
}
